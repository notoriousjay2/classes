#include <string>

using namespace std;

class OnlineVid
 {
    private:  //properties
         unsigned views;
         string id ; 
         


    public:  //methods
        
         OnlineVid();//default constructor 
		 OnlineVid(unsigned val, string a); //overloaded constructor
		 
          void setViews(unsigned);
          unsigned getViews();
          void setId(string);
          string getId();

 };


